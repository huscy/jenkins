# Jenkins

To start Jenkins just run

    $ docker-compose up -d

and wait until Jenkins is fully up and running. Open your Webbrowser and navigate to **http://localhost:8080**.



## Initial setup

When you navigate to **http://localhost:8080** for the first time Jenkins will ask for the administrator password.
The page tells you to find the initial password in a log file, but Jenkins prints the initial password to standard output too.
So, you can retrieve it from the Docker log.

    $ docker logs jenkins | less

Look for a block enclosed with six lines of asterisks like this:

    > *************************************************************
    > *************************************************************
    > *************************************************************
    >
    > Jenkins initial setup is required. An admin user has been created and a password generated.
    > Please use the following password to proceed to installation:
    >
    > c061b679107a4893b5383617729b5c6a
    >
    > This may also be found at: /var/jenkins_home/secrets/initialAdminPassword
    >
    > *************************************************************
    > *************************************************************
    > *************************************************************

Enter the password and click **Continue**.

Select **Select plugins to install** on the next page.
On the next page select the following plugins and click **Install**:

- SSH Build Agents

When Jenkins finishes, it will prompt you for a new admin user and password. Enter a user name and password and click **Save and Continue**.

The next page gives you a chance to change the host name of your controller. You can accept the default and click **Save and Finish**.

Jenkins is ready, now! Click **Start using Jenkins**.


## Add a Jenkins Agent

First of all you need to generate a SSH key to allow the controller to access the agent via SSH.

    $ ssh-keygen -t rsa -f ~/.ssh/jenkins_agent

Open Jenkins in your Browser and click **Manage Jenkins**.
Then go to **Manage Credentials**.
Click **Jenkins** under **Stores scoped to Jenkins**.
Then click **Global credentials**.
Finally, click **Add Credentials** in the menu on the left.

Set these options on this screen:

1. Select **SSH Username with private key**.
2. Limit the scope to System. This means the key can't be used for jobs.
3. Give the credential an ID (e.g. jenkins_agent)
4. Provide a description
5. Enter **jenkins** for a username. Don't use the username used to create the key.
6. Under **Private Key**, check **Enter directly**.
7. Paste the private key (contents of **~/.ssh/jenkins_agengt** file) in the text box.

Click **OK** to save the credential. Now it's time to set up the agent.

Go back to **Manage Jenkins** page and select **Manage Nodes and Clouds**.
Click **New Node**
Now define your Jenkins agent:

1. Add **jenkins_agent** as name.
2. Select **Permanent Agent**
3. Click **Create**

In the subsequent form set the **Remote root directory** to**/home/jenkins/agent**.
Then, in the next part of the form, select **Use this node as much as possible** under **Usage**.
Under **Launch method**, select **Launch agents via SSH**.
For **Host**, enter **agent**. Each docker container can reach the others by using their container names as hostnames.
Next, click the dropdown under **Credentials** and select the **jenkins** credentials you just defined.
Now, under **Host Key Verification Strategy**, select **Non verifying Verification Strategy**.
Click **Save** at the bottom.

Then click on **Build-In Node** under **Manage nodes and clouds**.
In the menu on the left click **Configure**.
Set the **Number of executers** to 0 and confirm with **Save**.
